﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Day2
{
    public class AdventOfCode2
    {
        static void Main(string[] args)
        {
            var paperRequired = CalculatePaperRequiredForInputFile();
            Console.WriteLine("The elves require {0}sqft of christmas paper", paperRequired);

            var ribbonRequired = CalculateRibbonRequiredForInputFile();
            Console.WriteLine("The elves require {0}ft of ribbon", ribbonRequired);
            
            Console.ReadLine();
        }

        public static int CalculatePaperRequiredForPresent(int length, int width, int height)
        {
            var side1 = length * width;
            var side2 = width * height;
            var side3 = height * length;

            var sides = new int[] { side1, side2, side3 };

            var extraPaper = sides.Min();

            var presentArea = (side1 * 2) + (side2 * 2) + (side3 * 2);

            return presentArea + extraPaper;
        }

        public static int CalculateRibbonRequiredForPresent(int length, int width, int height)
        {
            var perimitters = new int[] { 
                (length + width) * 2,
                (width + height) * 2,
                (height + length) * 2 };

            var ribbon = perimitters.Min();

            var bow = length * width * height;

            return ribbon + bow;
        }

        private static int CalculatePaperRequiredForInputFile()
        {
            var assembly = Assembly.GetExecutingAssembly();

            var input = assembly.GetManifestResourceStream("Day2.Input.Day2.txt");

            var presentList = new List<String>();

            using (var streamReader = new StreamReader(input))
            {
                string present;
                while ((present = streamReader.ReadLine()) != null)
                {
                    presentList.Add(present);
                }
            }

            var totalPaperRequired = 0;

            foreach (var present in presentList)
            {
                int length, width, height;

                var dimentions = present.Split('x');

                length = Int32.Parse(dimentions[0]);
                width = Int32.Parse(dimentions[1]);
                height = Int32.Parse(dimentions[2]);

                totalPaperRequired += CalculatePaperRequiredForPresent(length, width, height);
            }

            return totalPaperRequired;
        }

        private static int CalculateRibbonRequiredForInputFile()
        {
            var assembly = Assembly.GetExecutingAssembly();

            var input = assembly.GetManifestResourceStream("Day2.Input.Day2.txt");

            var presentList = new List<String>();

            using (var streamReader = new StreamReader(input))
            {
                string present;
                while ((present = streamReader.ReadLine()) != null)
                {
                    presentList.Add(present);
                }
            }

            var totalRibbonRequired = 0;

            foreach (var present in presentList)
            {
                int length, width, height;

                var dimentions = present.Split('x');

                length = Int32.Parse(dimentions[0]);
                width = Int32.Parse(dimentions[1]);
                height = Int32.Parse(dimentions[2]);

                totalRibbonRequired += CalculateRibbonRequiredForPresent(length, width, height);
            }

            return totalRibbonRequired;
        }
    }
}
