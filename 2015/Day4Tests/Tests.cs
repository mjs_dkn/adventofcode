﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Day4;

namespace Day4Tests
{
    [TestClass]
    public class Tests
    {
        [TestMethod, TestCategory("LongRunning")]
        public void Day4_Part1_Example1()
        {
            var hash = "";
            var secretKey = AdventOfCode4.FindSecretKey("abcdef", out hash, AdventOfCode4.FoundHashWith5LeadingZeros);

            Assert.AreEqual("609043", secretKey, "A hash of {0} should be found using a combination of input and expected result", hash);
        }

        [TestMethod, TestCategory("LongRunning")]
        public void Day4_Part1_Example2()
        {
            var hash = "";
            var secretKey = AdventOfCode4.FindSecretKey("pqrstuv", out hash, AdventOfCode4.FoundHashWith5LeadingZeros);

            Assert.AreEqual("1048970", secretKey, "A hash of {0} should be found using a combination of input and expected result", hash);
        }
    }
}
