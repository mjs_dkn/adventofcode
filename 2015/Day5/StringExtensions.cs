﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day5
{
    public static class StringExtensions
    {
        #region Part 1

        public static bool IsNicePartOne(this string s)
        {
            return
                NumberOfVowels(s) >= 3
                && ContainsAtleastOneLetterTwiceInARow(s)
                && DoesNotContain(s, "ab", "cd", "pq", "xy");
        }

        static int NumberOfVowels(string s)
        {
            var vowels = new[] { 'a', 'e', 'i', 'o', 'u' };
            var numberOfVowels = 0;

            foreach (var @char in s)
            {
                if (vowels.Contains(@char))
                    numberOfVowels++;
            }

            return numberOfVowels;
        }

        static bool ContainsAtleastOneLetterTwiceInARow(string s)
        {
            for (var i = 1; i < s.Length; i++)
            {
                if (s[i] == s[i - 1])
                    return true;
            }

            return false;
        }

        static bool DoesNotContain(string input, params string[] strings)
        {
            foreach (var s in strings)
            {
                if (input.Contains(s))
                    return false;
            }

            return true;
        }

        #endregion

        #region Part 2

        public static bool IsNicePartTwo(this string s)
        {
            return
                ContainsPairOfTwoLetters(s)
                && ContainsSplitRepeatingLetters(s);
        }

        static bool ContainsPairOfTwoLetters(string s)
        {
            var endPoint = 2;

            do
            {
                var startingPair = s[endPoint - 2].ToString() + s[endPoint - 1].ToString();

                if (SearchForPairs(s, new CharacterPair(startingPair, endPoint - 2), endPoint))
                    return true;
                else
                    endPoint++;

            } while (endPoint <= s.Length - 1);

            return false;
        }

        static bool SearchForPairs(string characters, CharacterPair pair, int endPoint)
        {
            if (endPoint == characters.Length)
                return false;

            var comparison = characters[endPoint - 1].ToString() + characters[endPoint].ToString();

            if (pair.CharString == comparison && !PairOverlaps(pair, new CharacterPair(comparison, endPoint - 1)))
                return true;

            return SearchForPairs(characters, pair, endPoint + 1);
        }

        private static bool PairOverlaps(CharacterPair search, CharacterPair comparison)
        {
            return comparison.StartPosition == search.EndPosition;
        }

        static bool ContainsSplitRepeatingLetters(string s)
        {
            for (var i = 2; i < s.Length; i++)
            {
                if (s[i] == s[i - 2])
                    return true;
            }

            return false;
        }

        #endregion        
    }
}
