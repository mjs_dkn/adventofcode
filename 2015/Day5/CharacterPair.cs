﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day5
{
    public class CharacterPair
    {
        public CharacterPair(string charString, int startPosition)
        {
            CharString = charString;
            StartPosition = startPosition;
        }

        public string CharString { get; set; }
        
        public int StartPosition { get; set; }

        public int EndPosition
        {
            get
            {
                return StartPosition + (CharString.Length - 1);
            }
        }
    }
}
