﻿using AdventOfCodeUtil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day5
{
    public class AdventOfCode5
    {
        static void Main(string[] args)
        {
            var numberOfNiceStrings = GetNumberOfNiceStringsFromInputFilePart1();
            Console.WriteLine("There are {0} nice strings - part 1", numberOfNiceStrings);

            var numberOfNiceStrings2 = GetNumberOfNiceStringsFromInputFilePart2();
            Console.WriteLine("There are {0} nice strings - part 2", numberOfNiceStrings2);

            Console.WriteLine("\nPress any key to exit...");
            Console.ReadLine();
        }

        public static int CalculateNumberOfNiceStringsPart1(IEnumerable<string> strings)
        {
            return strings.Count(s => s.IsNicePartOne());
        }

        public static int CalculateNumberOfNiceStringsPart2(IEnumerable<string> strings)
        {
            return strings.Count(s => s.IsNicePartTwo());
        }

        private static int GetNumberOfNiceStringsFromInputFilePart1()
        {
            var input = FileReaders.ReadAsStringList("Day5.Input.Day5.txt");
            return CalculateNumberOfNiceStringsPart1(input);
        }

        private static int GetNumberOfNiceStringsFromInputFilePart2()
        {
            var input = FileReaders.ReadAsStringList("Day5.Input.Day5.txt");
            return CalculateNumberOfNiceStringsPart2(input);
        }
    }
}
