﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Day5;
using AdventOfCodeUtil;
using System.Collections.Generic;

namespace Day5Tests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void Day5_Part1_Example1()
        {
            var isNice = "ugknbfddgicrmopn".IsNicePartOne();

            Assert.IsTrue(isNice);
        }

        [TestMethod]
        public void Day5_Part1_Example2()
        {
            var isNice = "aaa".IsNicePartOne();

            Assert.IsTrue(isNice);
        }

        [TestMethod]
        public void Day5_Part1_Example3()
        {
            var isNice = "jchzalrnumimnmhp".IsNicePartOne();

            Assert.IsFalse(isNice);
        }

        [TestMethod]
        public void Day5_Part1_Example4()
        {
            var isNice = "haegwjzuvuyypxyu".IsNicePartOne();

            Assert.IsFalse(isNice);
        }

        [TestMethod]
        public void Day5_Part1_Example5()
        {
            var isNice = "dvszwmarrgswjxmb".IsNicePartOne();

            Assert.IsFalse(isNice);
        }

        [TestMethod]
        public void Day5_Part2_Example1()
        {
            var isNice = "qjhvhtzxzqqjkmpb".IsNicePartTwo();

            Assert.IsTrue(isNice);
        }


        [TestMethod]
        public void Day5_Part2_Example2()
        {
            var isNice = "xxyxx".IsNicePartTwo();

            Assert.IsTrue(isNice);
        }

        [TestMethod]
        public void Day5_Part2_Example3()
        {
            var isNice = "uurcxstgmygtbstg".IsNicePartTwo();

            Assert.IsFalse(isNice);
        }

        [TestMethod]
        public void Day5_Part2_Example4()
        {
            var isNice = "abcabcb".IsNicePartTwo();

            Assert.IsTrue(isNice);
        }

        [TestMethod]
        public void Day5_Part2_Example5()
        {
            var isNice = "uurcxstgmygtbstg".IsNicePartTwo();

            Assert.IsFalse(isNice);
        }

        [TestMethod]
        public void Day5_Part2_Example6()
        {
            var isNice = "abxabmskgri".IsNicePartTwo();

            Assert.IsFalse(isNice);
        }

        [TestMethod]
        public void Day5_Part2_Example7()
        {
            var isNice = "abcdefghi".IsNicePartTwo();

            Assert.IsFalse(isNice);
        }

        [TestMethod]
        public void Day5_Part2_Example8()
        {
            var isNice = "acbxxjxyreacji".IsNicePartTwo();

            Assert.IsTrue(isNice);
        }

        [TestMethod]
        public void Day5_Part2_Example11()
        {
            var isNice = "abxab".IsNicePartTwo();

            Assert.IsFalse(isNice);
        }

        [TestMethod]
        public void Day5_Part2_Example12()
        {
            var isNice = "aaa".IsNicePartTwo();

            Assert.IsFalse(isNice);
        }

        [TestMethod]
        public void Day5_Part2_Example13()
        {
            var isNice = "xyxy".IsNicePartTwo();

            Assert.IsTrue(isNice);
        }

        [TestMethod]
        public void Day5_Part2_Example14()
        {
            var isNice = "aabcdefgaa".IsNicePartTwo();

            Assert.IsFalse(isNice);
        }

        [TestMethod]
        public void Day5_Part2_Example15()
        {
            var isNice = "xyx".IsNicePartTwo();

            Assert.IsFalse(isNice);
        }

        [TestMethod]
        public void Day5_Part2_Example16()
        {
            var isNice = "abcdefeghi".IsNicePartTwo();

            Assert.IsFalse(isNice);
        }

        [TestMethod]
        public void Day5_Part2_Example17()
        {
            var isNice = "qjhvhtzzzqqjkmpb".IsNicePartTwo();

            Assert.IsTrue(isNice);
        }

        [TestMethod]
        public void Day5_Part2_Example18()
        {
            var isNice = "acb".IsNicePartTwo();

            Assert.IsFalse(isNice);
        }

        [TestMethod]
        public void Day5_Part2_Example19()
        {
            var isNice = "qobacbob".IsNicePartTwo();

            Assert.IsTrue(isNice);
        }
    }
}
