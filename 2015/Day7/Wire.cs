﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7
{
    public class Wire
    {
        public string Name { get; set; }

        public ushort? Signal { get; set; }

        public Wire(string name)
        {
            Name = name;
        }
    }
}
