﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day7
{
    public class Instruction
    {
        public enum InstructionOperation
        {
            AND,
            NOT,
            OR,
            LSHIFT,
            RSHIFT,
            INPUT,
            PASSTHROUGH
        }

        public InstructionOperation Operation { get; set; }

        public Wire InputWire1 { get; set; }

        public Wire InputWire2 { get; set; }

        public int OperationValue { get; set; }

        public ushort InputValue { get; set; }

        public Wire OutputWire { get; set; }

        public bool ProcessedGate { get; set; }

        public void Process()
        {
            switch (Operation)
            {
                case Instruction.InstructionOperation.NOT:
                    if (ProcessedGate) break;
                    NOT();
                    break;
                case Instruction.InstructionOperation.OR:
                    if (ProcessedGate) break;
                    OR();
                    break;
                case Instruction.InstructionOperation.AND:
                    if (ProcessedGate) break;
                    AND();
                    break;
                case Instruction.InstructionOperation.LSHIFT:
                    if (ProcessedGate) break;
                    LSHIFT();
                    break;
                case Instruction.InstructionOperation.RSHIFT:
                    if (ProcessedGate) break;
                    RSHIFT();
                    break;
                case Instruction.InstructionOperation.INPUT:
                    INPUT();
                    break;
                case Instruction.InstructionOperation.PASSTHROUGH:
                    PASSTHROUGH();
                    break;
            }
        }

        void NOT()
        {
            if (!InputWire1.Signal.HasValue) return;

            OutputWire.Signal = (ushort)~InputWire1.Signal;
            ProcessedGate = true;
        }

        void OR()
        {
            if (!InputWire1.Signal.HasValue || !InputWire2.Signal.HasValue) return;

            OutputWire.Signal = (ushort)(InputWire1.Signal | InputWire2.Signal);
            ProcessedGate = true;
        }

        void AND()
        {
            if (InputValue != 0)
            {
                if (!InputWire1.Signal.HasValue) return;

                OutputWire.Signal = (ushort)(InputWire1.Signal & InputValue);
                ProcessedGate = true;
            }
            else
            {

                if (!InputWire1.Signal.HasValue || !InputWire2.Signal.HasValue) return;

                OutputWire.Signal = (ushort)(InputWire1.Signal & InputWire2.Signal);
                ProcessedGate = true;
            }
        }

        void LSHIFT()
        {
            if (!InputWire1.Signal.HasValue) return;

            OutputWire.Signal = (ushort)(InputWire1.Signal << OperationValue);
            ProcessedGate = true;
        }

        void RSHIFT()
        {
            if (!InputWire1.Signal.HasValue) return;

            OutputWire.Signal = (ushort)(InputWire1.Signal >> OperationValue);
            ProcessedGate = true;
        }

        void INPUT()
        {
            OutputWire.Signal = InputValue;
        }

        void PASSTHROUGH()
        {
            OutputWire.Signal = InputWire1.Signal;
        }
    }
}
