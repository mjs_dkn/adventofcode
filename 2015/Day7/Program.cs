﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Day7
{
    public class AdventOfCode7
    {
        static List<Wire> WireCollection = new List<Wire>();
        static List<Instruction> InstructionCollection = new List<Instruction>();

        static void Main(string[] args)
        {
            var instructions = AdventOfCodeUtil.FileReaders.ReadAsStringList("Day7.Input.Day7.txt");

            SetupCircuit(instructions);
            var wireASignal = TurnItOn("a");

            Reset();

            SetupCircuit(instructions);
            InstructionCollection.Single(x => x.OutputWire.Name == "b").InputValue = wireASignal;

            Console.WriteLine("Input signals changed..");
            TurnItOn("a");

            Console.WriteLine("Press any key to quit...");
            Console.ReadKey();
        }

        static void SetupCircuit(IEnumerable<string> instructions)
        {
            foreach (var instruction in instructions)
                ReadInstruction(instruction);
        }

        static void Reset()
        {
            WireCollection.Clear();
            InstructionCollection.Clear();
        }

        static ushort TurnItOn(string selectedWire)
        {
            var wire = WireCollection.FirstOrDefault(x => x.Name == selectedWire);

            do
            {
                InstructionCollection.ForEach(instruction => instruction.Process());
            } while (!wire.Signal.HasValue);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("The input signal for Wire {0} is {1}", wire.Name, wire.Signal.Value);
            Console.ResetColor();

            return wire.Signal.Value;
        }

        static void ReadInstruction(string input)
        {
            var instruction = new Instruction();
            var set = input.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            var operationRegEx = new Regex(@"\b[A-Z]+");
            var match = operationRegEx.Match(input);

            if (match.Success)
            {
                instruction.Operation = (Instruction.InstructionOperation)Enum.Parse(typeof(Instruction.InstructionOperation), match.Value);

                switch (instruction.Operation)
                {
                    case Instruction.InstructionOperation.NOT:
                        instruction.InputWire1 = GetOrCreateWire(set[1]);
                        instruction.OutputWire = GetOrCreateWire(set[3]);
                        break;
                    case Instruction.InstructionOperation.AND:
                    case Instruction.InstructionOperation.OR:
                        ushort inputValue;
                        if (ushort.TryParse(set[0], out inputValue))
                        {
                            instruction.InputValue = inputValue;
                            instruction.InputWire1 = GetOrCreateWire(set[2]);
                            instruction.OutputWire = GetOrCreateWire(set[4]);
                        }
                        else
                        {
                            instruction.InputWire1 = GetOrCreateWire(set[0]);
                            instruction.InputWire2 = GetOrCreateWire(set[2]);
                            instruction.OutputWire = GetOrCreateWire(set[4]);
                        }
                        break;
                    case Instruction.InstructionOperation.LSHIFT:
                    case Instruction.InstructionOperation.RSHIFT:
                        instruction.InputWire1 = GetOrCreateWire(set[0]);
                        instruction.OperationValue = Int32.Parse(set[2]);
                        instruction.OutputWire = GetOrCreateWire(set[4]);
                        break;
                }
            }
            else
            {
                ushort inputValue;
                if (ushort.TryParse(set[0], out inputValue))
                {
                    instruction.Operation = Instruction.InstructionOperation.INPUT;
                    instruction.InputValue = ushort.Parse(set[0]);
                    instruction.OutputWire = GetOrCreateWire(set[2]);
                }
                else
                {
                    instruction.Operation = Instruction.InstructionOperation.PASSTHROUGH;
                    instruction.InputWire1 = GetOrCreateWire(set[0]);
                    instruction.OutputWire = GetOrCreateWire(set[2]);
                }
            }


            InstructionCollection.Add(instruction);
        }

        static Wire GetOrCreateWire(string wireName)
        {
            var existingWire = WireCollection.FirstOrDefault(x => x.Name == wireName);

            if (existingWire != null)
                return existingWire;
            else
            {
                var newWire = new Wire(wireName);
                WireCollection.Add(newWire);

                return newWire;
            }
        }
    }
}
