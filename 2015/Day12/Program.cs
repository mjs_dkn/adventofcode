﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;

namespace Day12
{
    public class AdventOfCode12
    {
        static void Main(string[] args)
        {
            var input = AdventOfCodeUtil.FileReaders.ReadAsStringBlock("Day12.Input.Day12.txt");
            var numberValueOfJson = 0;

            using (JsonTextReader reader = new JsonTextReader(new StringReader(input)))
            {
                while (reader.Read())
                {
                    if (reader.ValueType == typeof(Int64))
                        numberValueOfJson += int.Parse(reader.Value.ToString());
                }
            }

            Console.WriteLine("The total value of the numbers in the json is: {0}", numberValueOfJson);

            dynamic dynamicJson = JsonConvert.DeserializeObject(input);

            var jObject = (JObject)dynamicJson;

            foreach (JToken token in jObject.Children())
            {
                if (token.Type == JTokenType.Property)
                    ParseProperty(token);
                if (token.Type == JTokenType.Array)
                    ParseArray(token);
            }

        }

        static void ParseProperty(JToken token)
        {
            foreach (var childToken in token.Children())
            {
                if (childToken.Type == JTokenType.Object)
                    ParseObject(childToken);
                else if (childToken.Type == JTokenType.Property)
                    ParseProperty(childToken);
                else if (childToken.Type == JTokenType.Array)
                    ParseArray(childToken);
            }
        }

        static void ParseArray(JToken token)
        {

        }

        static void ParseObject(JToken token)
        {

        }
    }
}
