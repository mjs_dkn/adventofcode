﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day3
{
    public class Position
    {
        public Position(int row, int column)
        {
            Row = row;
            Column = column;
        }

        public Position(Position referencePosition)
        {
            Row = referencePosition.Row;
            Column = referencePosition.Column;
        }

        public int Row { get; set; }
        public int Column { get; set; }

        public Position MoveNorth()
        {
            return new Position(this.Row + 1, this.Column);
        }

        public Position MoveSouth()
        {
            return new Position(this.Row - 1, this.Column);
        }

        public Position MoveEast()
        {
            return new Position(this.Row, this.Column + 1);
        }

        public Position MoveWest()
        {
            return new Position(this.Row, this.Column - 1);
        }
    }

    public class PositionComparer : IEqualityComparer<Position>
    {

        public bool Equals(Position x, Position y)
        {
            return (x.Row == y.Row && x.Column == y.Column);
        }

        public int GetHashCode(Position obj)
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + obj.Column;
                hash = hash * 23 + obj.Row;
                return hash;
            }
        }
    }
}
