﻿using AdventOfCodeUtil;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Day3
{
    public class AdventOfCode3
    {
        static void Main(string[] args)
        {
            var housesVisitedAtLeastOnce = CalculateHousesVisitedAtLeastOnceForInputFile();
            Console.WriteLine("Santa visted {0} houses at least once", housesVisitedAtLeastOnce);

            var housesVisitedBySantaAndRoboSantaAtLeastOnce = CalculateHousesVisitedBySantaAndRoboSantaAtLeastOnceForInputFile();
            Console.WriteLine("Santa and Robo-Santa visited {0} houses at least once", housesVisitedBySantaAndRoboSantaAtLeastOnce);
            
            Console.ReadLine();
        }

        public static int CalculateHousesVisitedAtLeastOnce(string directions)
        {
            var steps = directions.ToCharArray();
            var visitedLocations = new List<Position>();
            var startingPosition = new Position(0, 0);

            visitedLocations.Add(startingPosition);

            var curerentPosition = new Position(startingPosition);

            for(var i = 0; i < steps.Length; i++)
            {
                Move(steps[i], ref curerentPosition, visitedLocations);
            }

            return visitedLocations.Distinct(new PositionComparer()).Count();
        }

        public static int CalculateHousesVisitedWithRoboSantaAtLeastOnce(string directions)
        {
            var steps = directions.ToCharArray();
            var visitedLocations = new List<Position>();
            var santasStartingPosition = new Position(0, 0);
            var roboStartingPosition = new Position(0, 0);

            visitedLocations.Add(santasStartingPosition);
            visitedLocations.Add(roboStartingPosition);

            var santasCurrentPosition = new Position(santasStartingPosition);
            var robosCurrentPosition = new Position(roboStartingPosition);

            Position santasNewPosition;
            Position robosNewPosition;

            for(var i = 0; i < steps.Length; i++)
            {
                if (i % 2 == 0)
                    Move(steps[i], ref santasCurrentPosition, visitedLocations);
                else
                    Move(steps[i], ref robosCurrentPosition, visitedLocations);
            }

            return visitedLocations.Distinct(new PositionComparer()).Count();
        }

        private static void Move(char direction, ref Position deliverersCurrentPosition, List<Position> visitedLocations)
        {
            Position newPosition = null;
            switch (direction)
            {
                case '^':
                    newPosition = deliverersCurrentPosition.MoveNorth();
                    break;
                case '<':
                    newPosition = deliverersCurrentPosition.MoveWest();
                    break;
                case '>':
                    newPosition = deliverersCurrentPosition.MoveEast();
                    break;
                case 'v':
                    newPosition = deliverersCurrentPosition.MoveSouth();
                    break;
            }

            visitedLocations.Add(newPosition);
            deliverersCurrentPosition = newPosition;
        }

        private static int CalculateHousesVisitedAtLeastOnceForInputFile()
        {
            var input = FileReaders.ReadAsStringBlock("Day3.Input.Day3.txt");
            return CalculateHousesVisitedAtLeastOnce(input);
        }

        private static int CalculateHousesVisitedBySantaAndRoboSantaAtLeastOnceForInputFile()
        {
            var input = FileReaders.ReadAsStringBlock("Day3.Input.Day3.txt");
            return CalculateHousesVisitedWithRoboSantaAtLeastOnce(input);
        }
    }
}
