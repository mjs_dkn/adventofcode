﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day8
{
    public static class StringExtensions
    {
        public static int GetMemoryLength(this string s)
        {
            return Regex.Replace(s.Trim('"').Replace("\\\"", "*").Replace("\\\\", "*"), "\\\\x[a-f0-9]{2}", "*")
                .Length;          
        }

        public static int GetEncodedLength(this string s)
        {
            return s.Replace("\\", "**").Replace("\"", "**")
                .Length + 2;
        }
    }
}
