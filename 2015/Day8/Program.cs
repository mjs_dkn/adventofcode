﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day8
{
    public class AdventOfCode8
    {
        static void Main(string[] args)
        {
            var inputList = AdventOfCodeUtil.FileReaders.ReadAsStringList("Day8.Input.Day8.txt");

            var part1Answer = CalculateInputValue(inputList);
            var part2Answer = CalculateInputValueWithEncodedStrings(inputList);

            Console.WriteLine("Value of the input file part 1 is {0}", part1Answer);
            Console.WriteLine("Value of the input file part 2 is {0}", part2Answer);
            Console.ReadKey();
        }

        public static int CalculateInputValue(IEnumerable<string> inputList)
        {
            var numberOfCodeCharacters = 0;
            var numberOfMemoryCharacters = 0;

            foreach (var @string in inputList)
            {
                numberOfCodeCharacters += @string.Length;
                numberOfMemoryCharacters += @string.GetMemoryLength();
            }

            return numberOfCodeCharacters - numberOfMemoryCharacters;
        }

        public static int CalculateInputValueWithEncodedStrings(IEnumerable<string> inputList)
        {
            var numberOfCodeCharacters = 0;
            var numberOfEncodedCharacters = 0;

            foreach (var @string in inputList)
            {
                numberOfCodeCharacters += @string.Length;
                numberOfEncodedCharacters += @string.GetEncodedLength();
            }

            return numberOfEncodedCharacters - numberOfCodeCharacters;
        }
    }
}
