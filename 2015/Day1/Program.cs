﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Day1
{
    public class AdventOfCode1
    {
        #region Entry Point
        public static void Main(string[] args)
        {
            var floor = CalculateFloorPositionFromInputFile();
            Console.WriteLine("Santa need to deliver to floor {0}", floor);

            var basementFloorEnteredOnPosition = CalculateWhenEnteredBasementFromInputFile();
            Console.WriteLine("Santa entered the basement at position {0}", basementFloorEnteredOnPosition);
            
            Console.ReadLine();
        }
        #endregion

        #region Public Methods

        #region Part 1
        public static int CalculateEnteringBasement(string inputCode)
        {
            var floor = 0;
            var inputArray = inputCode.Select(x => x.ToString()).ToArray();
            var inputPosition = 0;

            foreach (var character in inputArray)
            {
                if (character == "(")
                {
                    floor++;
                }
                else
                {
                    floor--;
                }

                inputPosition++;

                if (floor < 0)
                    return inputPosition;
            }

            return -1;
        }
        #endregion

        #region Part 2
        public static int CalculateFloor(string inputCode)
        {
            var floor = 0;
            var inputArray = inputCode.Select(x => x.ToString()).ToArray();

            foreach (var character in inputArray)
            {
                if (character == "(")
                {
                    floor++;
                }
                else
                {
                    floor--;
                }
            }

            return floor;
        }
        #endregion

        #endregion

        #region Private Methods
        private static int CalculateFloorPositionFromInputFile()
        {
            var assembly = Assembly.GetExecutingAssembly();

            var input = assembly.GetManifestResourceStream("Day1.Input.Day1.txt");
            var inputString = new StringBuilder();

            using (var streamReader = new StreamReader(input))
            {
                while (streamReader.Peek() >= 0)
                {
                    inputString.Append(Convert.ToChar(streamReader.Read()));
                }
            }

            return CalculateFloor(inputString.ToString());
        }

        private static int CalculateWhenEnteredBasementFromInputFile()
        {
            var assembly = Assembly.GetExecutingAssembly();

            var input = assembly.GetManifestResourceStream("Day1.Input.Day1.txt");
            var inputString = new StringBuilder();

            using (var streamReader = new StreamReader(input))
            {
                while (streamReader.Peek() >= 0)
                {
                    inputString.Append(Convert.ToChar(streamReader.Read()));
                }
            }

            return CalculateEnteringBasement(inputString.ToString());
        }
        #endregion
    }
}
