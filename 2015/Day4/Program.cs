﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Day4
{
    public class AdventOfCode4
    {
        static void Main(string[] args)
        {
            string hash1, hash2;
            Console.WriteLine("Calculating Secret Keys\n");

            var part1_secretKey = FindSecretKey("iwrupvqb", out hash1, FoundHashWith5LeadingZeros);

            Console.WriteLine("Part 1 Hash found using value: {0} [{1}]", part1_secretKey, hash1);

            var part2_secretKey = FindSecretKey("iwrupvqb", out hash2, FoundHashWith6LeadingZeros);
            Console.WriteLine("Part 2 Hash found using value: {0} [{1}]", part2_secretKey, hash2);

            Console.WriteLine("\nPress any key to exit..");
            Console.ReadLine();
        }

        public static string FindSecretKey(string input, out string hash, Func<string, bool> FoundHashFunc)
        {
            var key = 0;

            do
            {

                key++;
                hash = CalculateMD5Hash(input + key);
                
                Console.WriteLine(hash);
                Console.SetCursorPosition(0, Console.CursorTop == 0 ? 0 : Console.CursorTop - 1);

            } while (!FoundHashFunc(hash));

            return key.ToString();
        }

        public static bool FoundHashWith5LeadingZeros(string hash)
        {
            return hash.StartsWith("00000");
        }

        public static bool FoundHashWith6LeadingZeros(string hash)
        {
            return hash.StartsWith("000000");
        }

        public static string CalculateMD5Hash(string input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }
    }
}
