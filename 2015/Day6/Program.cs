﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day6
{
    public class AdventOfCode6
    {
        static void Main(string[] args)
        {
            var instructions = "Day6.Input.Day6.txt";

            var commandManager = new CommandManager();
            commandManager.LoadCommandsFromFile(instructions);

            var lightGrid = new LightGrid();

            do
            {
                lightGrid.ExecuteCommand(commandManager.CommandQueue.Dequeue(), false);

            } while (commandManager.CommandQueue.Any());

            Console.WriteLine("The total number of lights turned on is {0}", lightGrid.GetLightsOnCount());

            lightGrid.Reset();

            commandManager.LoadCommandsFromFile(instructions);

            do
            {
                lightGrid.ExecuteCommand(commandManager.CommandQueue.Dequeue(), true);

            } while (commandManager.CommandQueue.Any());

            Console.WriteLine("The total brightness of the lights turned on is {0}", lightGrid.GetTotalBrightness());

            Console.WriteLine("\nPress any key to quit...");
            Console.ReadLine();
        }
    }
}
