﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Day6
{
    public class CommandManager
    {
        public Queue<Command> CommandQueue { get; set; }

        public CommandManager()
        {
            CommandQueue = new Queue<Command>();
        }

        public void LoadCommandsFromFile(string path)
        {
            var commandList = AdventOfCodeUtil.FileReaders.ReadAsStringList(path);
            ParseCommands(commandList);
        }

        void ParseCommands(IEnumerable<string> commandList)
        {
            foreach (var command in commandList)
                CommandQueue.Enqueue(Command.CreateFromString(command));
        }
    }

    public class Command
    {
        public enum CommandOperation
        {
            TurnOn,
            TurnOff,
            Toggle,
            Unknown
        }

        public Command(CommandOperation operation, Point startPoint, Point endPoint)
        {
            Operation = operation;
            Start = startPoint;
            End = endPoint;
        }

        public Point Start { get; set; }

        public Point End { get; set; }

        public CommandOperation Operation { get; set; }

        public static Command CreateFromString(string commandString)
        {
            CommandOperation operation = CommandOperation.Unknown;

            if (commandString.Contains("turn off"))
                operation = CommandOperation.TurnOff;
            else if (commandString.Contains("turn on"))
                operation = CommandOperation.TurnOn;
            else if (commandString.Contains("toggle"))
                operation = CommandOperation.Toggle;

            var dataPointString = commandString.Substring(commandString.IndexOfAny("0123456789".ToCharArray()));
            var dataPoints = dataPointString.Split(new[] {"through"}, StringSplitOptions.RemoveEmptyEntries);

            var startPoint = new Point(Int32.Parse(dataPoints[0].Split(',')[0]), Int32.Parse(dataPoints[0].Split(',')[1]));
            var endPoint = new Point(Int32.Parse(dataPoints[1].Split(',')[0]), Int32.Parse(dataPoints[1].Split(',')[1]));

            return new Command(operation, startPoint, endPoint);
        }
    }
}
