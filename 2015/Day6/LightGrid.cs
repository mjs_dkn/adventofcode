﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day6
{
    public class LightGrid
    {
        public int[,] Grid = new int[1000, 1000];

        public LightGrid()
        {
            Reset();
        }

        public void ExecuteCommand(Command command, bool useBrightnessControls)
        {
            for (var x = command.Start.X; x <= command.End.X; x++)
            {
                for (var y = command.Start.Y; y <= command.End.Y; y++)
                {
                    switch (command.Operation)
                    {
                        case Command.CommandOperation.TurnOn:
                            Grid[x, y]++;
                            break;
                        case Command.CommandOperation.TurnOff:
                            if (Grid[x, y] == 0) break;
                            Grid[x, y]--;
                            break;
                        case Command.CommandOperation.Toggle:
                            if (useBrightnessControls)
                            {
                                Grid[x, y] += 2;
                            }
                            else
                            {
                                if (Grid[x, y] == 0)
                                    Grid[x, y] = 1;
                                else
                                    Grid[x, y] = 0;
                            }
                            break;
                    }
                }
            }
        }

        public int GetLightsOnCount()
        {
            var lightsOn = 0;

            for (var x = 0; x < Grid.GetLength(0); x++)
            {
                for (var y = 0; y < Grid.GetLength(1); y++)
                {
                    if (Grid[x, y] >= 1)
                        lightsOn++;
                }
            }

            return lightsOn;
        }

        public int GetTotalBrightness()
        {
            var totalBrightness = 0;

            for (var x = 0; x < Grid.GetLength(0); x++)
            {
                for (var y = 0; y < Grid.GetLength(1); y++)
                {
                    totalBrightness += Grid[x, y];
                }
            }

            return totalBrightness;
        }

        public void Reset()
        {
            for (var x = 0; x < Grid.GetLength(0); x++)
            {
                for (var y = 0; y < Grid.GetLength(1); y++)
                {
                    Grid[x, y] = 0;
                }
            }
        }
    }
}
