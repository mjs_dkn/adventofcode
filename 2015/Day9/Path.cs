﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day9
{
    public class Path
    {
        public Path(string from, string to, int distance)
        {
            From = new Node(from);
            To = new Node(to);
            Distance = distance;
        }

        public Node From { get; set; }

        public Node To { get; set; }

        public int Distance { get; set; }

        public static Path Parse(string pathString)
        {
            var components = pathString.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            var from = components[0];
            var to = components[2];
            var distance = Int32.Parse(components[4]);

            return new Path(from, to, distance);
        }
    }
}
