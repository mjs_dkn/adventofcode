﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day9
{
    public class AdventOfCode9
    {
        static void Main(string[] args)
        {
            var fileInput = AdventOfCodeUtil.FileReaders.ReadAsStringList("Day9.Input.Day9.txt");

            var pathCollection = new PathCollection();

            Console.WriteLine("Calculating Routes...\n\r");

            foreach (var path in fileInput)
                pathCollection.Add(Path.Parse(path));

            var allPossibleRoutes = RouteCollection.GetAllPossibleRoutes(pathCollection);
            var shortestRoute = allPossibleRoutes.Aggregate((a, b) => a.TotalDistance < b.TotalDistance ? a : b);
            var longestRoute = allPossibleRoutes.Aggregate((a, b) => a.TotalDistance > b.TotalDistance ? a : b);

            Console.WriteLine("Shortest route =\n\r{0} \n\rTotal Distance = [{1}]", shortestRoute, shortestRoute.TotalDistance);
            Console.WriteLine();
            Console.WriteLine("Longest route =\n\r{0} \n\rTotal Distance = [{1}]", longestRoute, longestRoute.TotalDistance);

            Console.Write("\n\rPress any key to quit...");
            Console.ReadKey();
        }
    }
}
