﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day9
{
    public class PathCollection
    {
        HashSet<Path> _routes;

        HashSet<Node> _possibleNodes;

        public List<Path> Routes
        {
            get
            {
                if (_routes == null)
                    _routes = new HashSet<Path>();

                return _routes.ToList();
            }
        }

        public PathCollection()
        {
            _routes = new HashSet<Path>();
            _possibleNodes = new HashSet<Node>();
        }

        public void Add(Path route)
        {
            _routes.Add(route);

            _possibleNodes.Add(route.From);
            _possibleNodes.Add(route.To);
        }

        public List<Node> GetNodeLocations()
        {
            return _possibleNodes.ToList();
        }
        
    }
}
