﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day9
{
    public class Route
    {
        public List<Node> NodesVisited { get; set; }

        public int TotalDistance { get; set; }

        public void VisitNode(Node node)
        {
            if (NodesVisited.Contains(node)) return;

            NodesVisited.Add(node);
        }

        public Route()
        {
            NodesVisited = new List<Node>();
        }

        public Route(Node[] nodes, PathCollection paths)
            : this()
        {
            foreach (var node in nodes)
            {
                VisitNode(node);
            }

            for (var i = 0; i < NodesVisited.Count - 1; i++)
            {
                var path = paths.Routes.Where(
                    n => 
                        (n.From.Equals(NodesVisited[i]) && n.To.Equals(NodesVisited[i + 1]))
                        || (n.To.Equals(NodesVisited[i]) && n.From.Equals(NodesVisited[i + 1])))
                    .First();

                TotalDistance += path.Distance;
            }
        }

        public override string ToString()
        {
            return String.Join("->", NodesVisited.Select(n => n.Name));
        }
    }
}
