﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day9
{
    public class RouteCollection
    {
        static List<Route> _routes;

        static RouteCollection()
        {
            _routes = new List<Route>();
        }

        public static List<Route> GetAllPossibleRoutes(PathCollection paths)
        {
            var nodes = paths.GetNodeLocations();

            var possibilities = Permutations(nodes.ToArray());

            foreach (var routePossibility in possibilities)
                _routes.Add(new Route(routePossibility, paths));

            return _routes;
        }

        public static IEnumerable<T[]> Permutations<T>(T[] nodes, int from = 0)
        {
            if (nodes.Length == from + 1)
                yield return nodes;
            else
            {
                foreach (var v in Permutations(nodes, from + 1))
                    yield return v;

                for (var i = from + 1; i < nodes.Length; i++)
                {
                    SwapValues(nodes, from, i);
                    foreach (var v in Permutations(nodes, from + 1))
                        yield return v;
                    SwapValues(nodes, from, i);
                }
            }
        }

        private static void SwapValues<T>(T[] values, int pos1, int pos2)
        {
            if (pos1 != pos2)
            {
                T tmp = values[pos1];
                values[pos1] = values[pos2];
                values[pos2] = tmp;
            }
        }
    }
}
