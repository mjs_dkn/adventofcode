﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Day10
{
    public class AdventOfCode10
    {
        static void Main(string[] args)
        {
            var input = "3113322113";
            var loopCount = 50;

            for (var i = 0; i < loopCount; i++)
                input = LookAndSay(input);

            Console.WriteLine("The length of the output after {0} iterations is: {1}", loopCount, input.Length);
            Console.Write("\n\rPress any key to quit...");
            Console.ReadKey();
        }

        static string LookAndSay(string number)
        {
            StringBuilder result = new StringBuilder();

            char repeat = number[0];
            number = number.Substring(1, number.Length - 1) + " ";
            int times = 1;

            foreach (char actual in number)
            {
                if (actual != repeat)
                {
                    result.Append(Convert.ToString(times) + repeat);
                    times = 1;
                    repeat = actual;
                }
                else
                {
                    times += 1;
                }
            }
            return result.ToString();
        }
    }
}
