﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Day2;

namespace Day2Tests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void Day2_Part1_Example1()
        {
            var paperRequired = AdventOfCode2.CalculatePaperRequiredForPresent(2, 3, 4);

            Assert.AreEqual(58, paperRequired);
        }

        [TestMethod]
        public void Day2_Part1_Example2()
        {
            var paperRequired = AdventOfCode2.CalculatePaperRequiredForPresent(1, 1, 10);

            Assert.AreEqual(43, paperRequired);
        }

        [TestMethod]
        public void Day2_Part2_Example1()
        {
            var bowRequired = AdventOfCode2.CalculateRibbonRequiredForPresent(2, 3, 4);

            Assert.AreEqual(34, bowRequired);
        }

        public void Day2_Part2_Example2()
        {
            var bowRequired = AdventOfCode2.CalculateRibbonRequiredForPresent(1, 1, 10);

            Assert.AreEqual(14, bowRequired);
        }
    }
}
