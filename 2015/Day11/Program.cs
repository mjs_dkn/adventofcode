﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Day11
{
    public class AdventOfCode11
    {
        static void Main(string[] args)
        {
            var secondPassword = FindNextPassword("cqjxjnds");

            Console.WriteLine("Santa's next password should be: {0}", secondPassword);

            var thirdPassword = FindNextPassword(secondPassword);

            Console.WriteLine("Santa's password password after that should be: {0}", thirdPassword);
            
            Console.Write("Press any key to quit...");
            Console.Read();
        }

        public static string FindNextPassword(string currentPassword)
        {
            var password = currentPassword;

            do
            {
                password = IncrementPassword(Reverse(password));

            } while (!ValidPassword(password));

            return password;
        }

        public static string IncrementPassword(string currentPassword)
        {
            var newPassword = new StringBuilder(currentPassword);

            for (var i = 0; i < currentPassword.Length; i++)
            {
                var charVal = (int)currentPassword[i];

                var newVal = charVal + 1;

                if (newVal <= (int)'z')
                {
                    var newChar = (char)newVal;
                    newPassword[i] = newChar;
                    break;
                }
                else
                {
                    var newChar = 'a';
                    newPassword[i] = newChar;

                    if (i == currentPassword.Length - 1)
                        newPassword.Append('a');
                }
            }

            return Reverse(newPassword.ToString());
        }

        static string Reverse(string input)
        {
            var charArray = input.ToCharArray();
            Array.Reverse(charArray);

            return new String(charArray);
        }

        #region Validation

        static bool ValidPassword(string password)
        {
            var condition1 = new Regex(@"((.)\2)");

            return condition1.Matches(password).Count >= 2
                && !Contains(password, 'i', 'o', 'l')
                && ContainsSucessiveCharacters(password);
        }

        static bool ContainsSucessiveCharacters(string input)
        {
            if (input.Length < 3) return false;

            for (var i = 2; i < input.Length; i++)
            {
                var characters = new[] { input[i - 2], input[i - 1], input[i] };

                if ((int)characters[0] == (int)characters[1] - 1 && (int)characters[1] == (int)characters[2] - 1)
                    return true;
            }

            return false;
        }

        static bool Contains(string input, params char[] characters)
        {
            foreach (var c in input)
                if (characters.Contains(c)) return true;

            return false;
        }

        #endregion

    }
}
