﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCodeUtil
{
    public class FileReaders
    {
        public static string ReadAsStringBlock(string path)
        {
            var assembly = Assembly.GetCallingAssembly();

            var input = assembly.GetManifestResourceStream(path);
            var inputString = new StringBuilder();

            using (var streamReader = new StreamReader(input))
            {
                while (streamReader.Peek() >= 0)
                {
                    inputString.Append(Convert.ToChar(streamReader.Read()));
                }
            }

            return inputString.ToString();
        }

        public static IEnumerable<string> ReadAsStringList(string path)
        {
            var assembly = Assembly.GetCallingAssembly();

            var input = assembly.GetManifestResourceStream(path);

            var stringList = new List<String>();

            using (var streamReader = new StreamReader(input))
            {
                string s;
                while ((s = streamReader.ReadLine()) != null)
                {
                    stringList.Add(s);
                }
            }

            return stringList;
        }
    }
}
