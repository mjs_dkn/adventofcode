﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Day3;

namespace Day3Tests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void Day3_Part1_Example1()
        {
            var housesVisitedAtLeastOnce = AdventOfCode3.CalculateHousesVisitedAtLeastOnce(">");

            Assert.AreEqual(2, housesVisitedAtLeastOnce);
        }

        [TestMethod]
        public void Day3_Part1_Example2()
        {
            var housesVisitedAtLeastOnce = AdventOfCode3.CalculateHousesVisitedAtLeastOnce("^>v<");

            Assert.AreEqual(4, housesVisitedAtLeastOnce);
        }

        [TestMethod]
        public void Day3_Part1_Example3()
        {
            var housesVisitedAtLeastOnce = AdventOfCode3.CalculateHousesVisitedAtLeastOnce("^v^v^v^v^v");

            Assert.AreEqual(2, housesVisitedAtLeastOnce);
        }

        [TestMethod]
        public void Day3_Part2_Example1()
        {
            var housesVisitedBySantaAndRoboAtLeastOnce = AdventOfCode3.CalculateHousesVisitedWithRoboSantaAtLeastOnce("^v");

            Assert.AreEqual(3, housesVisitedBySantaAndRoboAtLeastOnce);
        }

        [TestMethod]
        public void Day3_Part2_Example2()
        {
            var housesVisitedBySantaAndRoboAtLeastOnce = AdventOfCode3.CalculateHousesVisitedWithRoboSantaAtLeastOnce(">v<");

            Assert.AreEqual(3, housesVisitedBySantaAndRoboAtLeastOnce);
        }

        [TestMethod]
        public void Day3_Part2_Example3()
        {
            var housesVisitedBySantaAndRoboAtLeastOnce = AdventOfCode3.CalculateHousesVisitedWithRoboSantaAtLeastOnce("^v^v^v^v^v");

            Assert.AreEqual(11, housesVisitedBySantaAndRoboAtLeastOnce);
        }
    }
}
