﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Day1;

namespace Day1Tests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void Day1_Part1_Example1()
        {
            var inputString1 = "(())";
            var inputString2 = "()()";

            var result1 = AdventOfCode1.CalculateFloor(inputString1);
            var result2 = AdventOfCode1.CalculateFloor(inputString2);

            Assert.AreEqual(0, result1);
            Assert.AreEqual(0, result2);
        }

        [TestMethod]
        public void Day1_Part1_Example2()
        {
            var inputString1 = "(((";
            var inputString2 = "(()(()(";
            var inputString3 = "))(((((";

            var result1 = AdventOfCode1.CalculateFloor(inputString1);
            var result2 = AdventOfCode1.CalculateFloor(inputString2);
            var result3 = AdventOfCode1.CalculateFloor(inputString3);

            Assert.AreEqual(3, result1);
            Assert.AreEqual(3, result2);
            Assert.AreEqual(3, result3);
        }

        [TestMethod]
        public void Day1_Part1_Example3()
        {
            var inputString1 = "())";
            var inputString2 = "))(";

            var result1 = AdventOfCode1.CalculateFloor(inputString1);
            var result2 = AdventOfCode1.CalculateFloor(inputString2);

            Assert.AreEqual(-1, result1);
            Assert.AreEqual(-1, result2);
        }

        [TestMethod]
        public void Day1_Part1_Example4()
        {
            var inputString1 = ")))";
            var inputString2 = ")())())";

            var result1 = AdventOfCode1.CalculateFloor(inputString1);
            var result2 = AdventOfCode1.CalculateFloor(inputString2);

            Assert.AreEqual(-3, result1);
            Assert.AreEqual(-3, result2);
        }

        [TestMethod]
        public void Day1_Part2_Example1()
        {
            var inputString = ")";

            var result = AdventOfCode1.CalculateEnteringBasement(inputString);

            Assert.AreEqual(1, result);
        }

        [TestMethod]
        public void Day1_Part2_Example2()
        {
            var inputString = "()())";

            var result = AdventOfCode1.CalculateEnteringBasement(inputString);

            Assert.AreEqual(5, result);
        }
    }
}
