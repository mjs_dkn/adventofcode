﻿using System;

namespace Day06
{
    using System.Collections.Generic;
    using System.Linq;

    class Program
    {
        static void Main(string[] args)
        {
            var input = System.IO.File.ReadAllText("input.txt").Split('\t').Select(int.Parse).ToList();
            var configurations = new HashSet<string> {string.Join("\t", input)};

            var blocksInBanks = Loop(input, configurations);
            
            Console.WriteLine(configurations.Count);

            configurations.Clear();
            configurations.Add(blocksInBanks);
            Loop(input, configurations);
            
            Console.WriteLine(configurations.Count);    
        }

        private static string Loop(IList<int> input, ISet<string> configurations)
        {
            string blocksInBanks;
            do
            {
                blocksInBanks = Redistribute(input);
            } while (configurations.Add(blocksInBanks));

            return blocksInBanks;
        }


        private static string Redistribute(IList<int> input)
        {
            var targetBank = input.IndexOf(input.Max());
            var cycle = input[targetBank];
            input[targetBank] = 0;

            for (var i = 0; i < cycle; i++)
                input[(targetBank + 1 + i) % input.Count]++;

            return string.Join("\t", input);
        }
    }
}