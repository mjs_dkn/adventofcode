﻿using System;

namespace Day10
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    class Program
    {
        private const string Lengths = "183,0,31,146,254,240,223,150,2,206,161,1,255,232,199,88";
        private static int _currentPosition;
        private static int _skip;

        static void Main(string[] args)
        {
            Part1();
            Part2();
        }

        private static void Part1()
        {
            var sparseHash = Enumerable.Range(0, 256).ToArray();
           
            KnotHash(Lengths.Split(',').Select(int.Parse), sparseHash);
            Console.WriteLine(sparseHash[0] * sparseHash[1]);
        }

        private static void Part2()
        {
            var sparseHash = Enumerable.Range(0, 256).ToArray();
            _currentPosition = 0;
            _skip = 0;
            
            var standardLengthSuffixValues = new byte[] {17, 31, 73, 47, 23};
            var asciiInput = Encoding.ASCII.GetBytes(Lengths);
            var lengthSequence = asciiInput.Concat(standardLengthSuffixValues).ToArray();

            for (var i = 0; i < 64; i++)
            {
                KnotHash(lengthSequence.Select(b => (int)b), sparseHash);
            }

            var denseHash = Reduce(sparseHash, 16);
            var hex = GetHex(denseHash);
            
            Console.WriteLine(hex);
        }
        
        private static void KnotHash(IEnumerable<int> lengths, int[] input)
        {
            foreach (var length in lengths)
            {
                input = SelectAndReverse(input, _currentPosition, length);
                _currentPosition += (length + _skip++);
            }
        }

        private static int[] SelectAndReverse(int[] list, int currentPosition, int length)
        {
            var selection = new List<int>();

            for (var i = 0; i < length; i++)
            {
                selection.Add(list[(currentPosition + i) % list.Length]);
            }

            selection.Reverse();
            var sublist = selection.ToArray();

            for (var i = 0; i < length; i++)
            {
                list[(currentPosition + i) % list.Length] = sublist[i];
            }

            return list;
        }
        
        private static string GetHex(int[] denseHash)
        {
            var hex = new StringBuilder(denseHash.Length * 2);
            
            foreach (var i in denseHash)
                hex.AppendFormat("{0:x2}", (byte) i);
            
            return hex.ToString();
        }

        private static int[] Reduce(int[] sparseHash, int blockSize)
        {
            var blocks = new List<int>();

            for (var i = 0; i < blockSize; i++)
            {
                var block = sparseHash.Skip(i * blockSize).Take(blockSize).ToArray();
                blocks.Add(block.Aggregate(0, (current, length) => current ^ length));
            }

            return blocks.ToArray();
        }
    }
}