﻿using System;

namespace Day01
{
    using System.Linq;
    using System.Text.RegularExpressions;

    class Program
    {
        static void Main(string[] args)
        {
            Part1();
            Part2();
        }

        static void Part1()
        {
            var matches = Regex.Matches(Input.value, "([0-9])\\1+");

            var total = matches.Cast<Match>()
                .Sum(match => match.Value
                                  .Select(digit => (int) char.GetNumericValue(digit))
                                  .First() * (match.Length - 1));

            total += Input.value[0] == Input.value[Input.value.Length - 1]
                ? (int) char.GetNumericValue(Input.value[0])
                : 0;

            Console.WriteLine(total);
        }

        static void Part2()
        {
            var steps = Input.value.Length / 2;
            var total = 0;

            for (var i = 0; i < Input.value.Length - 1; i++)
            {
                var matchingDigit = Input.value[(i + steps) % Input.value.Length];
                total += Input.value[i] == matchingDigit
                    ? (int) char.GetNumericValue(Input.value[i])
                    : 0;
            }
            
            Console.WriteLine(total);
        }
    }
}