﻿using System;

namespace Day05
{
    using System.Linq;

    class Program
    {
        static void Main(string[] args)
        {
            Part1();
            Part2();
        }

        private static void Part1()
        {
            var maze = System.IO.File.ReadLines("input.txt").Select(int.Parse).ToArray();
            var steps = 0;
            var currPos = 0;

            while (currPos >= 0 && currPos < maze.Length)
            {
                currPos += maze[currPos]++;
                steps++;
            }

            Console.WriteLine(steps);
        }

        private static void Part2()
        {
            var maze = System.IO.File.ReadLines("input.txt").Select(int.Parse).ToArray();
            var steps = 0;
            var currPos = 0;

            while (currPos >= 0 && currPos < maze.Length)
            {
                var jump = maze[currPos];
                maze[currPos] += jump > 2 ? -1 : 1;
                currPos += jump;
                steps++;
            }

            Console.WriteLine(steps);
        }
    }
}