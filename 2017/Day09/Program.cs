﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Day09
{
    class Program
    {
        static void Main(string[] args)
        {
            int garbageRemoved;
            var input = System.IO.File.ReadAllText("input.txt");
            var cancelledInput = RemoveCancelledCharacters(input);
            var cleanInput = RemoveGarbage(cancelledInput, out garbageRemoved);

            Console.WriteLine(CountGroups(cleanInput));
            Console.WriteLine(garbageRemoved);
        }

        private static string RemoveGarbage(string input, out int totalRemoved)
        {
            totalRemoved = 0;
            var pattern = @"\<(.*?)\>";
            var regex = new Regex(pattern);
            foreach (Match match in Regex.Matches(input, pattern))
            {
                totalRemoved += (match.Value.Where(c => c != '!').Count() - 2);
            }
            return regex.Replace(input, "");
        }

        private static string RemoveCancelledCharacters(string input)
        {
            var sb = new StringBuilder(input);
            for (var i = 0; i < sb.Length - 1; i++)
            {
                if (sb[i] == '!')
                {
                    sb.Remove(i + 1, 1);
                }
            }

            return sb.ToString();
        }

        private static int CountGroups(string input)
        {
            var groups = 0;
            var tier = 0;
            foreach (var @char in input)
            {
                if (@char == '{')
                {
                    tier++;
                }
                if (@char == '}')
                {
                    groups += tier;
                    tier--;
                }
            }
            return groups;
        }
    }
}
