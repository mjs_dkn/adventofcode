﻿using System;

namespace Day11
{
    using System.Linq;

    class Program
    {
        static void Main(string[] args)
        {
            var directions = System.IO.File.ReadAllText("input.txt").Split(',');
            var x = 0;
            var y = 0;
            var z = 0;
            var furthestDistance = 0;

            foreach (var direction in directions)
            {
                switch (direction)
                {
                        case "n": y++; z--;
                            break;
                        case "s": y--; z++;
                            break;
                        case "nw": y++; x--;
                            break;
                        case "ne": x++; z--;
                            break;
                        case "sw": x--; z++;
                            break;
                        case "se": y--; x++;
                            break;
                }

                furthestDistance = Math.Max(new[] {x, y, z}.Select(Math.Abs).Max(), furthestDistance);
            }

            var destination = HexPoint(x, y, z);
            var stepsToReach = (Math.Abs(0 - destination.X) + Math.Abs(0 - destination.Y) + Math.Abs(0 - destination.Z)) / 2;
            
            Console.WriteLine(stepsToReach);
            Console.WriteLine(furthestDistance);
        }

        private static (int X, int Y, int Z) HexPoint(int x, int y, int z)
        {
            return (x, y, z);
        }
    }
}