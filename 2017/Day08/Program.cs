﻿using System;

namespace Day08
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    class Program
    {
        static void Main(string[] args)
        {
            var instructions = System.IO.File.ReadAllLines("input.txt");
            const string pattern = @"^(\w+).(\w+).(-{0,}\d+).if.(\w+).([<>]=?|[!=]?=)\s(.+)";
            var registers = new Dictionary<string, int>();
            var memoryAlloc = 0;
            
            foreach (var instruction in instructions)
            {
                var match = Regex.Match(instruction, pattern);
                var register = match.Groups[1].Value;
                var operation = match.Groups[2].Value;
                var operationValue = int.Parse(match.Groups[3].Value);
                var conditionRegister = match.Groups[4].Value;
                var conditionOperation = match.Groups[5].Value;
                var conditionValue = int.Parse(match.Groups[6].Value);

                if (!registers.ContainsKey(register))
                    registers.Add(register, 0);
                
                if (!registers.ContainsKey(conditionRegister))
                    registers.Add(conditionRegister, 0);
                

                if (Compare(conditionOperation, registers[conditionRegister], conditionValue))
                {
                    if (operation == "dec")
                        registers[register] -= operationValue;
                    else
                        registers[register] += operationValue;

                    memoryAlloc = Math.Max(memoryAlloc, registers[register]);
                }
            }
            
            Console.WriteLine(registers.Values.Max());
            Console.WriteLine(memoryAlloc);
        }

        static bool Compare(string operation, int left, int right)
        {
            switch (operation.Trim())
            {
                case ">": return left.CompareTo(right) > 0;
                case "<": return left.CompareTo(right) < 0;
                case ">=": return left.CompareTo(right) >= 0;
                case "==": return left.Equals(right);
                case "<=": return left.CompareTo(right) <= 0;
                case "!=": return !left.Equals(right);
                default: throw new Exception("Invalid operation");
            }
        }
    }
}