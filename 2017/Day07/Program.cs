﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day07
{
    class Program
    {
        
        
        static void Main(string[] args)
        {       
            Part1();
            Part2();
        }

        private static void Part1()
        {
            Console.WriteLine(GetRoot().Name);
        }
        
        private static void Part2()
        {
            var firstDiscWithBalancedTowers = GetFirstBalancedDisc(GetRoot().Heaviest());
            var unbalancedTowerWeights = firstDiscWithBalancedTowers.Parent.Children.Select(c => c.TowerWeight).Distinct().ToArray();
            var towerDiff = Math.Abs(unbalancedTowerWeights[0] - unbalancedTowerWeights[1]);
            var unbalancedProgramOffset = firstDiscWithBalancedTowers.OwnWeight - towerDiff;
            
            Console.WriteLine(unbalancedProgramOffset);
        }
        
        private static Disc GetRoot()
        {
            const string pattern = @"(\w+)\s(\(\d+\))(\s\->\s(.+)){0,}";
            var input = System.IO.File.ReadAllLines("input.txt");
            var discs = new List<Disc>();
            
            foreach (var program in input)
            {
                var match = Regex.Match(program, pattern);
                var programName = match.Groups[1].Value;
                var programWeight = int.Parse(match.Groups[2].Value.Replace("(", "").Replace(")", ""));
                var programChildren = match.Groups[4];
                
                var disc = GetOrCreate(discs, programName);
                disc.OwnWeight = programWeight;

                if (programChildren.Length > 0)
                {
                    programChildren.Value.Split(',')
                        .Select(x => x.Trim())
                        .Select(c => GetOrCreate(discs, c))
                        .ToList()
                        .ForEach(c => disc.AddChild(c));
                }
            }
            
            return discs.Single(x => x.Parent == null);
        }

        private static Disc GetFirstBalancedDisc(Disc tower)
        {
            while (true)
            {
                var isBalanced = tower.Children.Select(x => x.TowerWeight).Distinct().Count() == 1;
                if (isBalanced) return tower;
                
                tower = tower.Heaviest();
            }
        }
        
        private static Disc GetOrCreate(ICollection<Disc> discs, string name)
        {           
            var disc = discs.FirstOrDefault(d => d.Name == name);
            if (disc != null)
            {
                return disc;
            }

            disc = new Disc(name);
            discs.Add(disc);

            return disc;
        }
    }

    internal class Disc
    {
        public Disc(string name)
        {
            Name = name;
        }

        public readonly ICollection<Disc> Children = new List<Disc>();
        public string Name { get; }
        public int OwnWeight;
        public Disc Parent { get; private set; }
        public int TowerWeight => CalculateBalance();
        
        public void AddChild(Disc child)
        {
            child.Parent = this;
            Children.Add(child);
        }

        public Disc Heaviest()
        {
            return Children.Aggregate((a, b) => a.TowerWeight > b.TowerWeight ? a : b);
        }

        private int CalculateBalance()
        {
            return OwnWeight + Children.Sum(c => c.CalculateBalance());
        }
        
        
    }
}