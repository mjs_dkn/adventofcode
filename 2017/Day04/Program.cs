﻿using System;

namespace Day04
{
    using System.IO;
    using System.Linq;

    class Program
    {
        static void Main(string[] args)
        {
            Part1();
            Part2();
        }    

        private static void Part1()
        {
            var validPassphrases = 0;
            string line;
            var file = File.OpenText("input.txt");

            while ((line = file.ReadLine()) != null)
            {
                var split = line.Split(' ');
                if (split.GroupBy(x => x).Count() == split.Length)
                    validPassphrases++;
            }

            Console.WriteLine(validPassphrases);
        }
        
        private static void Part2()
        {
            var validPassphrases = 0;
            string line;
            var file = File.OpenText("input.txt");

            while ((line = file.ReadLine()) != null)
            {
                var split = line.Split(' ');
                if (split
                    .Select(p => string.Concat(p.OrderBy(c => c)))
                    .GroupBy(p => p)
                    .Count() == split.Length)
                {
                    validPassphrases++;
                }
                
            }
            
            Console.WriteLine(validPassphrases);
        }
    }
}