﻿using System;
using System.Collections.Generic;

namespace Day1
{
    public class Position
    {

        private Direction _currentDirection;
        private int _x;
        private int _y;

        public Position(int x, int y)
        {
            _x = x;
            _y = y;

            _currentDirection = Direction.North;
        }

        private Position(int x, int y, DateTime dateVisited) : this(x, y)
        {
            VisitedOn = dateVisited;
        }

        public DateTime VisitedOn { get; }

        public List<Position> Visisted { get; } = new List<Position>();

        public int GetDistanceFromStartPoint()
        {
            return GetDistance(_x, _y);
        }

        private static int GetDistance(int x, int y)
        {
            var absX = Math.Abs(x);
            var absY = Math.Abs(y);

            return absX + absY;
        }

        public void TurnRight()
        {
            if (_currentDirection == Direction.West)
            {
                _currentDirection = Direction.North;
            }
            else
            {
                _currentDirection++;
            }
        }

        public void TurnLeft()
        {
            if (_currentDirection == Direction.North)
            {
                _currentDirection = Direction.West;
            }
            else
            {
                _currentDirection--;
            }
        }

        public void Move(int distance)
        {
            for (var i = 0; i < distance; i++)
            {
                switch (_currentDirection)
                {
                    case Direction.North:
                        _y++;
                        break;
                    case Direction.East:
                        _x++;
                        break;
                    case Direction.South:
                        _y--;
                        break;
                    case Direction.West:
                        _x--;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                Visit();
            }
        }

        private void Visit()
        {
            Visisted.Add(new Position(_x, _y, DateTime.Now));
        }

        private bool Equals(Position other)
        {
            return (_x == other._x) && (_y == other._y);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            return (obj.GetType() == GetType()) && Equals((Position) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (_x*397) ^ _y;
            }
        }

        private enum Direction
        {
            North = 1,
            East = 2,
            South = 3,
            West = 4
        }
    }
}