﻿using System;
using System.Linq;
using Util;

namespace Day1
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var commands =
                Util.Util.GetOperations(
                    "R2, L5, L4, L5, R4, R1, L4, R5, R3, R1, L1, L1, R4, L4, L1, R4, L4, R4, L3, R5, R4, R1, R3, L1, L1, R1, L2, R5, L4, L3, R1, L2, L2, R192, L3, R5, R48, R5, L2, R76, R4, R2, R1, L1, L5, L1, R185, L5, L1, R5, L4, R1, R3, L4, L3, R1, L5, R4, L4, R4, R5, L3, L1, L2, L4, L3, L4, R2, R2, L3, L5, R2, R5, L1, R1, L3, L5, L3, R4, L4, R3, L1, R5, L3, R2, R4, R2, L1, R3, L1, L3, L5, R4, R5, R2, R2, L5, L3, L1, L1, L5, L2, L3, R3, R3, L3, L4, L5, R2, L1, R1, R3, R4, L2, R1, L1, R3, R3, L4, L2, R5, R5, L1, R4, L5, L5, R1, L5, R4, R2, L1, L4, R1, L1, L1, L5, R3, R4, L2, R1, R2, R1, R1, R3, L5, R1, R4");

            var startingPoint = new Position(0, 0);

            foreach (var command in commands)
            {
                var direction = command[0];
                var distance = command.Substring(1);

                if (direction == 'L')
                {
                    startingPoint.TurnLeft();
                }
                else
                {
                    startingPoint.TurnRight();
                }

                startingPoint.Move(int.Parse(distance));
            }

            // Part 1
            Console.WriteLine(startingPoint.GetDistanceFromStartPoint());

            // Part 2
            Console.WriteLine(
                startingPoint.Visisted
                    .GroupBy(x => x)
                    .OrderBy(x => x.Key.VisitedOn)
                    .First(PositionVistedTwice())
                    .Select(x => x.GetDistanceFromStartPoint())
                    .First());
        }

        private static Func<IGrouping<Position, Position>, bool> PositionVistedTwice()
        {
            return g => g.Count() >= 2;
        }
    }
}