﻿using System.Collections.Generic;
using System.Linq;

namespace Day10
{
    public class Bot
    {
        public Bot(int id)
        {
            Id = id;
            HoldingChips = new List<Chip>();
        }

        public int Id { get; private set; }

        public ICollection<Chip> HoldingChips { get; }

        public bool IsHoldingTwoChips
        {
            get
            {
                return HoldingChips.Count == 2;
            }
        }

        public Chip HighChip
        {
            get { return HoldingChips.OrderByDescending(c => c.Value).FirstOrDefault(); }
        }

        public Chip LowChip
        {
            get { return HoldingChips.OrderBy(c => c.Value).FirstOrDefault(); }
        }

        public void InsertValue(int value)
        {
            HoldingChips.Add(new Chip(value));
        }

        public void GiveHighChipToBot(Bot bot)
        {
            bot.HoldingChips.Add(HighChip);
            HoldingChips.Remove(HighChip);
        }

        public void GiveLowChipToBot(Bot bot)
        {
            bot.HoldingChips.Add(LowChip);
            HoldingChips.Remove(LowChip);
        }

        public void GiveHighChipToOutput(Output output)
        {
            output.Value += HighChip.Value;
            HoldingChips.Remove(HighChip);
        }

        public void GiveLowChipToOutput(Output output)
        {
            output.Value += LowChip.Value;
            HoldingChips.Remove(LowChip);
        }
    }
}