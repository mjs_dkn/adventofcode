﻿namespace Day10
{
    public class Output
    {
        public Output(int id)
        {
            Id = id;
        }

        public int Value { get; set; }
        public int Id { get; set; }
    }
}