﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Day10
{
    internal class Program
    {
        private static readonly HashSet<Bot> bots = new HashSet<Bot>();
        private static readonly HashSet<Output> outputs = new HashSet<Output>();


        private static void Main(string[] args)
        {

            var instructions = File.ReadAllLines("day10.txt");
            var values = instructions.Where(instruction => instruction.Contains("value")).ToList();
            var botInstructions = new List<Instruction>();
            botInstructions.AddRange(instructions.Except(values).Select(i => new Instruction(i)));

            foreach (var valueInstruction in values)
            {
                var instruction = valueInstruction.Split(' ');
                var chipValue = int.Parse(instruction[1]);
                var botId = int.Parse(instruction[5]);

                var bot = GetBot(botId);
                bot.InsertValue(chipValue);
                bots.Add(bot);
            }

            do
            {
                foreach (var botInstruction in botInstructions)
                {
                    var instruction = botInstruction.InstructionString.Split(' ');
                    var sourceBot = GetBot(int.Parse(instruction[1]));


                    if (sourceBot.IsHoldingTwoChips)
                    {
                        if ((sourceBot.LowChip.Value == 17) && (sourceBot.HighChip.Value == 61))
                        {
                            Console.WriteLine($"{sourceBot.Id}");
                        }
                        // Process Low Chip
                        var lowChipDestId = int.Parse(instruction[6]);
                        var lowChipTarget = instruction[5] == "bot"
                            ? (object) GetBot(lowChipDestId)
                            : GetOutput(lowChipDestId);

                        if (lowChipTarget is Bot)
                        {
                            sourceBot.GiveLowChipToBot((Bot) lowChipTarget);
                        }
                        else
                        {
                            sourceBot.GiveLowChipToOutput((Output) lowChipTarget);
                        }

                        // Process High Chip
                        var highChipTargetId = int.Parse(instruction[11]);
                        var highChipTarget = instruction[10] == "bot"
                            ? (object) GetBot(highChipTargetId)
                            : GetOutput(highChipTargetId);

                        if (highChipTarget is Bot)
                        {
                            sourceBot.GiveHighChipToBot((Bot) highChipTarget);
                        }
                        else
                        {
                            sourceBot.GiveHighChipToOutput((Output) highChipTarget);
                        }

                        botInstruction.Processed = true;
                    }
                }
            } while (!botInstructions.TrueForAll(x => x.Processed));

            var total = outputs.Where(x => (x.Id == 0) || (x.Id == 1) || (x.Id == 2))
                .Select(x => x.Value)
                .Aggregate(1, (x, y) => x*y);
            Console.WriteLine($"{total}");
        }

        public static Bot GetBot(int id)
        {
            var bot = bots.FirstOrDefault(b => b.Id == id);

            if (bot != null)
            {
                return bot;
            }

            var newBot = new Bot(id);
            bots.Add(newBot);
            return newBot;
        }

        public static Output GetOutput(int id)
        {
            var output = outputs.FirstOrDefault(o => o.Id == id);

            if (output != null)
            {
                return output;
            }

            var newOutput = new Output(id);
            outputs.Add(newOutput);
            return newOutput;
        }
    }
}