﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day10
{
    public class Instruction
    {
        public Instruction(string instruction)
        {
            InstructionString = instruction;
            Processed = false;
        }
        public bool Processed { get; set; }

        public string InstructionString { get; private set; }
    }
}
