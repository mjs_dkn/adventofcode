﻿using System;
using System.Linq;
using System.Text;

namespace Day6
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var columns = Util.Util.ReadAsColumns("day6.txt");

            var message = new StringBuilder();
            var modifiedMessage = new StringBuilder();

            foreach (var column in columns)
            {
                message.Append(column.GroupBy(l => l).OrderByDescending(l => l.Count()).First().Key);
                modifiedMessage.Append(column.GroupBy(l => l).OrderBy(l => l.Count()).First().Key);
            }

            Console.WriteLine(message.ToString());
            Console.WriteLine(modifiedMessage.ToString());
        }
    }
}