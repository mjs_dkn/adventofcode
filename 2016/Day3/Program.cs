﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Day3
{
    class Program
    {
        static void Main(string[] args)
        {
            // Part 1
            var input = File.ReadAllLines("day3.txt");
            var possibleTriangles = 0;

            foreach (var design in input)
            {
                if (IsATriangle(design))
                    possibleTriangles++;
            }


            Console.WriteLine(possibleTriangles);

            // Part 2
            var part2Input = File.ReadAllText("day3.txt");
            var allSides = part2Input.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToArray();
            var part2Designs = new List<string>();

            for (var i = 0; i <= allSides.Length - 9; i += 9)
            {
                for (var t = 0; t < 3; t++)
                {
                    part2Designs.Add($"{allSides[t + i]} {allSides[(t + i) + 3]} {allSides[(t + i) + 6]}");
                }
            }

            possibleTriangles = 0;
            foreach (var design in part2Designs)
            {
                if (IsATriangle(design))
                    possibleTriangles++;
            }

            Console.WriteLine(possibleTriangles);
        }

        private static bool IsATriangle(string design)
        {
            var sides = design.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).Select(s => int.Parse(s)).ToArray();

            return
                (sides[0] + sides[1] > sides[2])
                && (sides[0] + sides[2] > sides[1])
                && (sides[1] + sides[2] > sides[0]);
        }
    }
}
