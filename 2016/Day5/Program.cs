﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Day5
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var input = "abc";

            Console.WriteLine("Run which Part? (1|2)");
            var partToExectute = Console.ReadKey();
            Console.Clear();
            Console.WriteLine("Working...");

            if (partToExectute.Key == ConsoleKey.D1)
            {
                var password = GetPassword(input, SimpleGeneration, new List<string>());
                Console.WriteLine(password);
            }
            else
            {
                var password = GetPassword(input, ComplexGeneration, Enumerable.Repeat("", 8).ToList());
                Console.WriteLine(password);
            }
        }

        private static string GetPassword(string input, Action<List<string>, string> generatePassword, List<string> password)
        {
            var key = 0;
            do
            {
                string hash;
                do
                {
                    key++;
                    hash = CalculateMd5Hash(input + key);

                } while (!FoundHashWith5LeadingZeros(hash));

                generatePassword(password, hash);

            } while (PasswordIsNotFilled(password));

            return string.Join("", password);
        }

        private static bool PasswordIsNotFilled(List<string> passwordCharacters)
        {
            return (passwordCharacters.Count < 8) || passwordCharacters.Any(x => x == "");
        }

        private static void SimpleGeneration(List<string> password, string hash)
        {
            password.Add(hash.Substring(5, 1));
        }

        private static void ComplexGeneration(List<string> password, string hash)
        {
            int characterIndex;
            var validPosition = int.TryParse(hash.Substring(5, 1), out characterIndex);

            if (!validPosition || (characterIndex < 0) || (characterIndex > 7) || (password[characterIndex] != ""))
            {
                return;
            }

            var character = hash.Substring(6, 1);

            password[characterIndex] = character;
        }

        private static bool FoundHashWith5LeadingZeros(string hash)
        {
            return hash.StartsWith("00000");
        }

        private static string CalculateMd5Hash(string input)
        {
            var md5 = MD5.Create();
            var inputBytes = Encoding.ASCII.GetBytes(input);
            var hash = md5.ComputeHash(inputBytes);

            var sb = new StringBuilder();
            foreach (var @byte in hash)
            {
                sb.Append(@byte.ToString("X2"));
            }

            return sb.ToString();
        }
    }
}