﻿using System;
using System.IO;
using System.Linq;

namespace Day9
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var input = File.ReadAllText("day9.txt");
            Console.WriteLine($"{Decompress(input.ToCharArray(), 1)}");
        }

        static long Decompress(char[] compressed, long repeat)
        {
            var length = (long)0;

            for (var i = 0; i < compressed.Length; i++)
            {
                if (compressed[i] == '(')
                {
                    var marker = new string(compressed.Skip(i + 1).TakeWhile(character => character != ')').ToArray());
                    var markerValues = marker.Split('x');
                    var numberOfCharacters = int.Parse(markerValues[0]);
                    var skip = i + marker.Length + 2;
                    length += Decompress(compressed.Skip(skip).Take(numberOfCharacters).ToArray(), int.Parse(markerValues[1]));
                    i = skip + numberOfCharacters - 1;
                }
                else
                    length++;
            }
            return length * repeat;
        }
    }
}