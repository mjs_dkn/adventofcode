﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Day4
{
    class Program
    {
        static void Main(string[] args)
        {
            var rooms = System.IO.File.ReadAllLines("day4.txt");
            var totalSectorId = 0;
            var roomNames = new List<string>();

            foreach (var room in rooms)
            {
                try
                {
                    totalSectorId += GetSectorId(room);
                    roomNames.Add(DecryptRoomName(room));
                }
                catch (NotRealRoomException) { }
            }

            // Part 1
            Console.WriteLine(totalSectorId);

            // Part 2
            roomNames.Where(r => r.Contains("north")).ToList().ForEach(r => Console.WriteLine(r));
        }

        private static int GetSectorId(string room)
        {
            if (!IsRealRoom(room))
                throw new NotRealRoomException();

            return int.Parse(Regex.Match(room, "[0-9]+").Value);
        }

        private static bool IsRealRoom(string room)
        {
            var encryptedName = room.Substring(0, room.LastIndexOf("-")).Replace("-", "");
            var checkSum = Regex.Match(room, "[[a-z]+]").Value.Replace("[", "").Replace("]", "");

            var commonLetters = encryptedName.GroupBy(l => l).OrderByDescending(g => g.Count()).ThenBy(l => l.First())
                .Take(checkSum.Length)
                .Select(x => x.First())
                .ToArray();

            return new string(commonLetters) == checkSum;
        }

        private static string DecryptRoomName(string room)
        {
            var sectorId = int.Parse(Regex.Match(room, "[0-9]+").Value);
            var decryptedName = new StringBuilder();

            foreach (var character in room.Substring(0, room.LastIndexOf("-")))
            {
                decryptedName.Append(RotateCharacter(character, sectorId));
            }

            decryptedName.Append($"-{sectorId}");
            return decryptedName.ToString();
        }

        private static char RotateCharacter(char character, int rotations)
        {
            for (var i = 0; i < rotations; i++)
            {
                switch (character)
                {
                    case 'z':
                        character = 'a';
                        continue;
                    case '-':
                        character = ' ';
                        continue;
                    case ' ':
                        character = '-';
                        continue;
                    default:
                        character++;
                        break;
               }
            }

            return character;
        }
    }

    class NotRealRoomException : Exception
    {

    }
}
