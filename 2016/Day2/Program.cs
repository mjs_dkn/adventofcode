﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Day2
{
    class Program
    {
        static void Main(string[] args)
        {
            var input =
                @"ULUULLUULUUUUDURUUULLDLDDRDRDULULRULLRLULRUDRRLDDLRULLLDRDRRDDLLLLDURUURDUDUUURDRLRLLURUDRDULURRUDLRDRRLLRDULLDURURLLLULLRLUDDLRRURRLDULRDDULDLRLURDUDRLLRUDDRLRDLLDDUURLRUDDURRLRURLDDDURRDLLDUUDLLLDUDURLUDURLRDLURURRLRLDDRURRLRRDURLURURRRULRRDLDDDDLLRDLDDDRDDRLUUDDLDUURUULDLUULUURRDRLDDDULRRRRULULLRLLDDUDRLRRLLLLLDRULURLLDULULLUULDDRURUDULDRDRRURLDRDDLULRDDRDLRLUDLLLDUDULUUUUDRDRURDDULLRDRLRRURLRDLRRRRUDDLRDDUDLDLUUDLDDRRRDRLLRLUURUDRUUULUDDDLDUULULLRUDULULLLDRLDDLLUUDRDDDDRUDURDRRUUDDLRRRRURLURLD
                LDLUDDLLDDRLLDLDRDDDDDUURUDDDUURLRLRLDULLLDLUDDDULLDUDLRUUDDLUULLDRLDDUDLUDDLURRRLDUURDDRULLURLLRLLUUDRLDDDLDLDRDUDLRDURULDLDRRDRLDLUURRRRLUDDULDULUUUDULDDRLLDDRRUULURRUURRLDUUUDDDDRUURUDRRRDDDDLRLURRRRUUDDDULRRURRDLULRURDDRDRLUDLURDDRDURRUURDUDUDRRDDURRRDURDLUUUURRUDULLDDRLLLURLDUDRRLDDLULUDUDDDDUDLUUULUURUDRURUUDUUURRLDUUDRDRURLLDLLLLLRLLUDURDRRLULRRDDDRLDRDDURLRDULULLDDURURLRRDRULDULUUUURLDURUDUDUDDLUDRRDURULRDULLLDRRDLDLUDURDULULLDDURDDUDRUUUDUDRLDUURDUUUDUURURUDRULRURLDLRDDURDLUU
                DDLDRLLDRRDRRLLUUURDDULRDUDRDRUDULURLLDDLRRRUDRDLDLURRRULUDRDLULLULLDUUDRLRUDDLRRURRUULRLDLLLDLRLLLURLLLURLLRDDLULLDUURLURDLLDLDUDLDRUUUDDLLDRRRRRUDRURUURRRDRUURDRDDRLDUUULUDUDRUDLLLLDRDRURRRDUUURLDLRLRDDDRLUDULDRLLULRDLDURDLDURUUDDULLULRDDRLRUURLLLURDRUURUUDUUULRDUDDRDURRRDUUDRRRUDRDLRURDLLDDDURLLRRDDDDLDULULDRLDRULDDLRRRLUDLLLLUDURRRUURUUULRRLDUURDLURRLRLLRDLRDDRDDLRDLULRUUUDDDUDRRURDDURURDDUDLURUUURUUUUDURDDLDRDULDRLDRLLRLRRRLDRLLDDRDLDLUDDLUDLULDLLDRDLLRDULDUDDULRRRUUDULDULRRURLRDRUDLDUDLURRRDDULRDDRULDLUUDDLRDUURDRDR
                URDURRRRUURULDLRUUDURDLLDUULULDURUDULLUDULRUDUUURLDRRULRRLLRDUURDDDLRDDRULUUURRRRDLLDLRLRULDLRRRRUDULDDURDLDUUULDURLLUDLURULLURRRDRLLDRRDULUDDURLDULLDURLUDUULRRLLURURLDLLLURDUDRLDDDRDULLUDDRLDDRRRLDULLLLDUURULUDDDURUULUUUDURUDURDURULLLDRULULDRRLDRLDLRLRUDUDURRLURLRUUDRRDULULDLLDRDRRRDUDUURLDULLLURRDLUDDLDDRDDUDLDDRRRUDRULLURDDULRLDUDDDRULURLLUDLLRLRRDRDRRURUUUURDLUURRDULLRDLDLRDDRDRLLLRRDDLDDDDLUDLRLULRRDDRDLDLUUUDLDURURLULLLDDDULURLRRURLDDRDDLD
                UDUULLRLUDLLUULRURRUUDDLLLDUURRURURDDRDLRRURLLRURLDDDRRDDUDRLLDRRUDRDRDDRURLULDDLDLRRUDDULLRLDDLRURLUURUURURDLDUDRLUUURRRLUURUDUDUUDDLDULUULRLDLLURLDRUDRLLRULURDLDDLLULLDRRUUDDLRRRUDDLRDRRRULDRDDRRULLLUDRUULURDUDRDLRRLDLRLRLDDULRRLULUUDDULDUDDULRRURLRDRDURUDDDLLRLDRDRULDDLLRLLRDUDDDDDDRLRLLDURUULDUUUDRURRLLRLDDDDRDRDUURRURDRDLLLUDDRDRRRDLUDLUUDRULURDLLLLLRDUDLLRULUULRLULRURULRLRRULUURLUDLDLLUURDLLULLLDDLRUDDRULRDLULRUURLDRULRRLULRLRULRDLURLLRURULRDRDLRRLRRDRUUURURULLLDDUURLDUDLLRRLRLRULLDUUUULDDUUU";


            // Part 1
            var imaginedKeyPad = new ImaginedKeyPad();
            ProcessInstructions(input, imaginedKeyPad);
            Console.WriteLine($"{imaginedKeyPad.Code}");

            // Part 2
            var actualKeyPad = new ActualKeyPad();
            ProcessInstructions(input, actualKeyPad);
            Console.WriteLine($"{actualKeyPad.Code}");
        }

        private static void ProcessInstructions(string input, KeyPad keyPad)
        {
            foreach (var line in input.Split('\r'))
            {
                foreach (var instruction in Regex.Replace(line, @"\s+", ""))
                {
                    keyPad.Move(instruction);
                }

                keyPad.Code += keyPad.FingerPosition.Value.ToString();
            }
        }
    }

    class KeyPad
    {
        public ICollection<Key> Keys { get; set; }

        public Key FingerPosition { get; set; }

        public string Code { get; set; }

        public void Move(char direction)
        {
            switch (direction)
            {
                case 'U':
                    if (string.IsNullOrEmpty(FingerPosition.MoveUp))
                        break;
                    else
                        FingerPosition = Keys.Single(key => key.Value == FingerPosition.MoveUp);
                    break;
                case 'D':
                    if (string.IsNullOrEmpty(FingerPosition.MoveDown))
                        break;
                    else
                        FingerPosition = Keys.Single(key => key.Value == FingerPosition.MoveDown);
                    break;
                case 'L':
                    if (string.IsNullOrEmpty(FingerPosition.MoveLeft))
                        break;
                    else
                        FingerPosition = Keys.Single(key => key.Value == FingerPosition.MoveLeft);
                    break;
                case 'R':
                    if (string.IsNullOrEmpty(FingerPosition.MoveRight))
                        break;
                    else
                        FingerPosition = Keys.Single(key => key.Value == FingerPosition.MoveRight);
                    break;
            }
        }
    }

    class ImaginedKeyPad : KeyPad
    {
        public ImaginedKeyPad()
        {
            Keys = new List<Key>();

            Keys.Add(new Key { Value = "1", MoveDown = "4", MoveRight = "2" });
            Keys.Add(new Key { Value = "2", MoveDown = "5", MoveRight = "3", MoveLeft = "1" });
            Keys.Add(new Key { Value = "3", MoveLeft = "2", MoveDown = "6" });
            Keys.Add(new Key { Value = "4", MoveRight = "5", MoveDown = "7", MoveUp = "1" });
            Keys.Add(new Key { Value = "5", MoveLeft = "4", MoveRight = "6", MoveUp = "2", MoveDown = "8" });
            Keys.Add(new Key { Value = "6", MoveLeft = "5", MoveDown = "9", MoveUp = "3" });
            Keys.Add(new Key { Value = "7", MoveRight = "8", MoveUp = "4" });
            Keys.Add(new Key { Value = "8", MoveRight = "9", MoveLeft = "7", MoveUp = "5" });
            Keys.Add(new Key { Value = "9", MoveLeft = "8", MoveUp = "6" });                                          

            FingerPosition = Keys.Single(key => key.Value == "5");
        }
    }

    class ActualKeyPad : KeyPad
    {
        public ActualKeyPad()
        {
            Keys = new List<Key>();

            Keys.Add(new Key { Value = "1", MoveDown = "3" });
            Keys.Add(new Key { Value = "2", MoveRight = "3", MoveDown = "6" });
            Keys.Add(new Key { Value = "3", MoveLeft = "2", MoveRight = "4", MoveUp = "4", MoveDown = "7" });
            Keys.Add(new Key { Value = "4", MoveLeft = "3", MoveDown = "8" });
            Keys.Add(new Key { Value = "5", MoveRight = "6" });
            Keys.Add(new Key { Value = "6", MoveLeft = "5", MoveRight = "7", MoveUp = "2", MoveDown = "A" });
            Keys.Add(new Key { Value = "7", MoveLeft = "6", MoveRight = "8", MoveUp = "3", MoveDown = "B" });
            Keys.Add(new Key { Value = "8", MoveLeft = "7", MoveRight = "9", MoveUp = "4", MoveDown = "C" });
            Keys.Add(new Key { Value = "9", MoveLeft = "8" });
            Keys.Add(new Key { Value = "A", MoveRight = "B", MoveUp = "6" });
            Keys.Add(new Key { Value = "B", MoveRight = "C", MoveUp = "7", MoveLeft = "A", MoveDown = "D" });
            Keys.Add(new Key { Value = "C", MoveUp = "8", MoveLeft = "B" });
            Keys.Add(new Key { Value = "D", MoveUp = "B" });

            FingerPosition = Keys.Single(key => key.Value == "5");
        }
    }

    class Key
    {
        public string Value { get; set; }

        public string MoveUp { get; set; }
        public string MoveDown { get; set; }
        public string MoveRight { get; set; }
        public string MoveLeft { get; set; }
    }
}
