﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Day7
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var ips = File.ReadAllLines("day7.txt");
            var tlsSupportingIps = 0;
            var sslSupportingIps = 0;

            foreach (var ip in ips)
            {
                if (IpSupportsTls(ip))
                {
                    tlsSupportingIps++;
                }

                if (IpSupportsSsl(ip))
                {
                    sslSupportingIps++;
                }
            }

            Console.WriteLine(tlsSupportingIps);
            Console.WriteLine(sslSupportingIps);
        }

        private static bool IpSupportsTls(string ip)
        {
            var sequenceList = new List<string>();
            for (var i = 1; i < ip.Length; i++)
            {
                if (ip[i] == ip[i - 1])
                {
                    var sequence = GetValidSequence(i - 1, ip);

                    if (!string.IsNullOrEmpty(sequence))
                    {
                        sequenceList.Add(sequence);
                    }
                }
            }

            foreach (var aba in sequenceList)
            {
                if (SequenceIsWithinHypernet(aba, ip))
                {
                    return false;
                }
            }

            return sequenceList.Count > 0;
        }

        private static bool IpSupportsSsl(string ip)
        {
            var ipWithoutHypernets = Regex.Replace(ip, "(\\[[a-z]+\\])", "");

            var abaList = new List<string>();

            for (var i = 0; i < ipWithoutHypernets.Length - 2; i++)
            {
                if ((ipWithoutHypernets[i] == ipWithoutHypernets[i + 2])
                    && (ipWithoutHypernets[i] != ipWithoutHypernets[i + 1]))
                {
                    var aba = ipWithoutHypernets.Substring(i, 3);

                    if (!aba.Contains("[") && !aba.Contains("]"))
                    {
                        abaList.Add(aba);
                    }
                }
            }

            foreach (var aba in abaList)
            {
                var bab = $"{aba[1]}{aba[0]}{aba[1]}";

                if (SequenceIsWithinHypernet(bab, ip))
                {
                    return true;
                }
            }

            return false;
        }

        private static string GetValidSequence(int indexOfMatchingPair, string ip)
        {
            if ((indexOfMatchingPair == 0) || (indexOfMatchingPair + 2 == ip.Length))
            {
                return string.Empty;
            }

            var characterBeforeMatchingPair = ip[indexOfMatchingPair - 1];
            var characterAfterMatchingPair = ip[indexOfMatchingPair + 2];

            if ((characterBeforeMatchingPair == characterAfterMatchingPair) &&
                (characterBeforeMatchingPair != ip[indexOfMatchingPair]))
            {
                return
                    $"{characterBeforeMatchingPair}{ip[indexOfMatchingPair]}{ip[indexOfMatchingPair]}{characterAfterMatchingPair}";
            }

            return string.Empty;
        }

        private static bool SequenceIsWithinHypernet(string sequence, string ip)
        {
            var hypernetMatch = Regex.Match(ip, $"[[a-z]{{0,}}{sequence}[a-z]{{0,}}]");
            return hypernetMatch.Success;
        }
    }
}