﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Principal;

namespace Util
{
    public static class Util
    {
        public static List<string> GetOperations(string commaSepratedInputs)
        {
            return commaSepratedInputs.Split(',').Select(x => x.Trim()).ToList();
        }

        public static List<List<string>> ReadAsColumns(string path)
        {
            var rows = File.ReadAllLines(path);
            var columnCount = rows.First().Length;
            var rowCount = rows.Length;
            var columns = new List<List<string>>();
            var grid = new string[rowCount, columnCount];

            for (var r = 0; r < rowCount; r++)
            {
                for (var c = 0; c < columnCount; c++)
                {
                    grid[r, c] = rows[r][c].ToString();
                }
            }

            for(var c = 0; c < columnCount; c++)
            {
                var column = new List<string>();

                for(var r = 0; r < rowCount; r++)
                {
                    column.Add(grid[r,c]);
                }

                columns.Add(column);
            }

            return columns;
        }
    }
}