﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

namespace Day8
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var littleScreen = new Screen(50, 6);

            littleScreen.ParseInstructions(File.ReadAllLines("day8.txt"));
            littleScreen.RunInstructionSet();

            Console.WriteLine(littleScreen.GetLitPixelCount());
        }
    }

    internal class Screen
    {
        private readonly int _height;

        private readonly ICollection<Instruction> _instuctionSet = new List<Instruction>();
        private readonly int _width;

        public Screen(int width, int height)
        {
            _width = width;
            _height = height;

            PixelGrid = new bool[width, height];
        }

        private bool[,] PixelGrid { get; }

        public int GetLitPixelCount()
        {
            return _instuctionSet.Where(i => i.GetType() == typeof(Rect))
                .Sum(x => x.A * x.B);
        }

        public void RunInstructionSet()
        {
            foreach (var instruction in _instuctionSet)
            {
                instruction.Run(this);

                Thread.Sleep(25);
                Console.Clear();
                Render();
            }
        }

        public void ParseInstructions(IEnumerable<string> instructions)
        {
            foreach (var instruction in instructions)
            {
                var instructionValues = Regex.Matches(instruction, "[0-9]+");
                var a = int.Parse(instructionValues[0].Value);
                var b = int.Parse(instructionValues[1].Value);

                _instuctionSet.Add(
                    instruction.Contains("rect")
                        ? (Instruction) new Rect(a, b)
                        : new Rotate(a, b, instruction.Contains("row") ? Rotate.Target.Row : Rotate.Target.Column));
            }
        }

        private void Render()
        {
            for (var r = 1; r <= _height; r++)
            {
                for (var c = 1; c <= _width; c++)
                {
                    Console.Write(PixelGrid[c - 1, r - 1] ? "#" : ".");
                }

                Console.WriteLine();
            }
        }

        private abstract class Instruction
        {
            public int A { get; set; }
            public int B { get; set; }

            public abstract void Run(Screen screen);
        }

        private class Rect : Instruction
        {
            public Rect(int a, int b)
            {
                A = a;
                B = b;
            }

            public override void Run(Screen screen)
            {
                for (var r = 1; r <= B; r++)
                {
                    for (var c = 1; c <= A; c++)
                    {
                        screen.PixelGrid[c - 1, r - 1] = true;
                    }
                }
            }
        }

        private class Rotate : Instruction
        {
            public enum Target
            {
                Row,
                Column
            }

            private readonly Target _target;

            public Rotate(int a, int b, Target target)
            {
                A = a;
                B = b;
                _target = target;
            }

            public override void Run(Screen screen)
            {

                switch (_target)
                {
                    case Target.Column:
                        for (var b = 0; b < B; b++)
                        {
                            ShiftColumnDown(screen.PixelGrid, A);
                        }
                        break;
                    case Target.Row:
                        for (var b = 0; b < B; b++)
                        {
                            ShiftRowRight(screen.PixelGrid, A);
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            private static void ShiftRowRight(bool[,] grid, int row)
            {
                var rollover = grid[grid.GetLength(0) - 1, row];

                for (var c = grid.GetLength(0) - 1; c > 0; c--)
                {
                    grid[c, row] = grid[c - 1, row];
                }

                grid[0, row] = rollover;
            }

            private static void ShiftColumnDown(bool[,] grid, int column)
            {
                var rollover = grid[column, grid.GetLength(1) - 1];

                for (var r = grid.GetLength(1) - 1; r > 0; r--)
                {
                    grid[column, r] = grid[column, r - 1];
                }

                grid[column, 0] = rollover;
            }
        }
    }
}